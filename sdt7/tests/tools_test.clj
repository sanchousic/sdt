(ns tools_test
  (:use expression)
  (:use tools)
  (:require [clojure.test :as test]))

; 1. Tests for the 'atom'
(test/is (atom? (constant 0)))
(test/is (atom? (variable :x)))
(test/is (not (atom? (conjunction (variable :x1) (variable :x2)))))
(test/is (not (atom? (disjunction (variable :x1) (variable :x2)))))
(test/is (not (atom? (implication (variable :x1) (variable :x2)))))
(test/is (not (atom? (negation (variable :x)))))

; 2. Tests for the 'negation-of'
(test/is ((negation-of constant?) (negation (constant 0))))
(test/is (not ((negation-of constant?) (negation (variable :x)))))

; 3. Tests for the conjuncts
; 3.1. Tests for the 'conjunct?'
(test/is (conjunct? (constant 0)))
(test/is (conjunct? (variable :x)))
(test/is (conjunct? (negation (constant 0))))
(test/is (conjunct? (negation (variable :x))))
(test/is (conjunct? (conjunction (constant 0) (variable :x) (negation (constant 0)) (negation (variable :x)))))
(test/is (not (conjunct? (disjunction (variable :x1) (variable :x2)))))
(test/is (not (conjunct? (negation (disjunction (variable :x1) (variable :x2))))))
(test/is (not (conjunct? (conjunction (variable :x1) (disjunction (variable :x2) (variable :x3))))))
(test/is (not (conjunct? (conjunction (variable :x1) (negation (disjunction (variable :x2) (variable :x3)))))))

; 3.2. Tests for the 'conjunct-expressions'
(let [atom (constant 0) negation_of_atom (negation atom)]
  (do
    (test/is (= (conjunct-expressions atom) (list atom)))
    (test/is (= (conjunct-expressions negation_of_atom) (list negation_of_atom)))
    (test/is (= (conjunct-expressions (conjunction atom negation_of_atom atom)) (list atom negation_of_atom atom)))))

; 3.3. Tests for the 'conjunct-set-of-constants-values'
(test/is (= (conjunct-set-of-constants-values (constant 0)) (hash-set 0)))
(test/is (= (conjunct-set-of-constants-values (constant 1)) (hash-set 1)))
(test/is (= (conjunct-set-of-constants-values (negation (constant 0))) (hash-set 1)))
(test/is (= (conjunct-set-of-constants-values (negation (constant 1))) (hash-set 0)))
(test/is (= (conjunct-set-of-constants-values (conjunction (constant 0) (constant 0) (constant 1) (constant 1))) (hash-set 0 1)))

; 3.4. Tests for the 'conjunct-set-of-variables-names'
(test/is (= (conjunct-set-of-variables-names (constant 0)) (hash-set)))
(test/is (= (conjunct-set-of-variables-names (negation (constant 0))) (hash-set)))
(test/is (= (conjunct-set-of-variables-names (variable :x)) (hash-set :x)))
(test/is (= (conjunct-set-of-variables-names (negation (variable :x))) (hash-set)))
(test/is (= (conjunct-set-of-variables-names (conjunction (variable :x1) (variable :x2) (negation (variable :x3)) (negation (variable :x4)) (constant 0) (constant 1))) (hash-set :x1 :x2)))

; 3.5. Tests for the 'conjunct-set-of-negated-variables-names'
(test/is (= (conjunct-set-of-negated-variables-names (constant 0)) (hash-set)))
(test/is (= (conjunct-set-of-negated-variables-names (negation (constant 0))) (hash-set)))
(test/is (= (conjunct-set-of-negated-variables-names (variable :x)) (hash-set)))
(test/is (= (conjunct-set-of-negated-variables-names (negation (variable :x))) (hash-set :x)))
(test/is (= (conjunct-set-of-negated-variables-names (conjunction (variable :x1) (variable :x2) (negation (variable :x3)) (negation (variable :x4)) (constant 0) (constant 1))) (hash-set :x3 :x4)))

; 3.6. Tests for the 'conjunct-unite'
(let [expr1 (constant 0) expr2 (negation (constant 1)) expr3 (variable :x1) expr4 (negation (variable :x2))]
  (let [united_conjunct (conjuncts-unite (conjunction expr1 expr2) (conjunction expr3 expr4))]
    (test/is (and (conjunction? united_conjunct) (= (conjunction-expressions united_conjunct) (list expr1 expr2 expr3 expr4))))))

; 3.7. Tests for the 'conjunct-normalize'
(test/is (let [normalized_conjunct (conjunct-normalize (conjunction (variable :x) (constant 0)))] (and (constant? normalized_conjunct) (= (constant-value normalized_conjunct) 0))))
(let [expr1 (variable :x1) expr2 (variable :x2) normalized_conjunct (conjunct-normalize (conjunction expr1 expr1 expr2))] (test/is (and (conjunction? normalized_conjunct) (= (conjunction-expressions normalized_conjunct) (list expr1 expr2)))))
(let [expr1 (negation (variable :x1)) expr2 (variable :x2) normalized_conjunct (conjunct-normalize (conjunction expr1 expr1 expr2))] (test/is (and (conjunction? normalized_conjunct) (= (conjunction-expressions normalized_conjunct) (list expr2 expr1)))))
(test/is (let [normalized_conjunct (conjunct-normalize (conjunction (variable :x1) (variable :x2) (negation (variable :x1)) (negation (variable :x3))))] (and (constant? normalized_conjunct) (= (constant-value normalized_conjunct) 0))))
(test/is (let [normalized_conjunct (conjunct-normalize (constant 1))] (and (constant? normalized_conjunct) (= (constant-value normalized_conjunct) 1))))
(test/is (let [normalized_conjunct (conjunct-normalize (variable :x))] (and (variable? normalized_conjunct) (= (variable-name normalized_conjunct) :x))))
