(ns expression_test
  (:use expression)
  (:require [clojure.test :as test]))


; 1. Tests for the 'constant'
(test/is (constant? (constant 0)))
(test/is (constant? (constant 1)))
(test/is (= (constant-value (constant 0)) 0))
(test/is (= (constant-value (constant 1)) 1))

; 2. Tests for the 'variable'
(test/is (variable? (variable :x)))
(test/is (= (variable-name (variable :x)) :x))

; 3. Tests for the 'conjunction'
(let [expr1 (variable :x1) expr2 (variable :x2) expr3 (variable :x3)]
  (do
    (test/is (conjunction? (conjunction expr1)))
    (test/is (conjunction? (conjunction expr1 expr2)))
    (test/is (conjunction? (conjunction expr1 expr2 expr3)))
    (test/is (= (conjunction-expressions (conjunction expr1 expr2 expr3)) (list expr1 expr2 expr3)))))

; 4. Tests for the 'disjunction'
(let [expr1 (variable :x1) expr2 (variable :x2) expr3 (variable :x3)]
  (do
    (test/is (disjunction? (disjunction expr1)))
    (test/is (disjunction? (disjunction expr1 expr2)))
    (test/is (disjunction? (disjunction expr1 expr2 expr3)))
    (test/is (= (disjunction-expressions (disjunction expr1 expr2 expr3)) (list expr1 expr2 expr3)))))

; 5. Tests for the 'implication'
(let [expr1 (variable :x1) expr2 (variable :x2)]
  (do
    (test/is (implication? (implication expr1 expr2)))
    (test/is (= (implication-expressions (implication expr1 expr2)) (list expr1 expr2)))))

; 6. Tests for the 'negation'
(let [expr (variable :x)]
  (do
    (test/is (negation? (negation expr)))
    (test/is (= (negation-expression (negation expr)) expr))))
