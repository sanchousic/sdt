(ns dnf_test
  (:use expression)
  (:use dnf)
  (:require [clojure.test :as test]))

; 1. Tests for the tools
; 1.1. Tests for the 'dnf?'
(test/is (dnf? (constant 0)))
(test/is (dnf? (variable :x)))
(test/is (dnf? (negation (constant 0))))
(test/is (dnf? (negation (variable :x))))
(test/is (not (dnf? (negation (disjunction (variable :x1) (constant 0))))))
(test/is (not (dnf? (negation (conjunction (variable :x1) (constant 1))))))
(test/is (dnf? (disjunction (constant 0) (variable :x1) (conjunction (constant 1) (variable :x2)))))
(test/is (not (dnf? (disjunction (constant 0) (variable :x1) (conjunction (disjunction (constant 0) (variable :x1)) (variable :x2))))))

; 1.2. Tests for the 'dnf-conjuncts'
(test/is (= (dnf-conjuncts (constant 0)) (list (constant 0))))
(test/is (= (dnf-conjuncts (negation (constant 0))) (list (negation (constant 0)))))
(test/is (= (dnf-conjuncts (conjunction (constant 0) (variable :x))) (list (conjunction (constant 0) (variable :x)))))

; 1.3. Tests for the 'dnfs-unite'
(let [expr1 (constant 0) expr2 (variable :x1) expr3 (conjunction (variable :x2) (variable :x3)) expr4 (negation (variable :x4)) expr5 (constant 0) expr6 (conjunction (negation (variable :x5)) (variable :x6))]
  (let [dnf (dnfs-unite (disjunction expr1 expr2 expr3) (disjunction expr4 expr5 expr6))]
    (and
      (disjunction? dnf)
      (=
        (apply hash-set (disjunction-expressions dnf))
        (hash-set
          (conjunction expr1 expr4)
          (conjunction expr1 expr5)
          (conjunction expr1 expr6)
          (conjunction expr2 expr4)
          (conjunction expr2 expr5)
          (conjunction expr2 expr6)
          (conjunction expr3 expr4)
          (conjunction expr3 expr5)
          (conjunction expr3 expr6))))))

; 1.4. Tests for the 'dnf-normalize'
(let [normalized_dnf (dnf-normalize (disjunction (constant 0) (conjunction (variable :x) (variable :y))))] (test/is (and (conjunction? normalized_dnf) (= (apply hash-set (conjunction-expressions normalized_dnf)) (hash-set (variable :x) (variable :y))))))
(test/is (= (dnf-normalize (disjunction (constant 1) (conjunction (variable :x) (variable :y)))) (constant 1)))
(let [normalized_dnf (dnf-normalize (disjunction (conjunction (variable :x) (variable :y)) (conjunction (variable :x) (variable :y))))] (test/is (and (conjunction? normalized_dnf) (= (apply hash-set (conjunction-expressions normalized_dnf)) (hash-set (variable :x) (variable :y))))))
(test/is (= (dnf-normalize (disjunction (variable :x) (negation (variable :x)) (conjunction (variable :x) (variable :y)))) (constant 1)))
(test/is (= (dnf-normalize (constant 0)) (constant 0)))

; 2. Tests for the 'expression-dnf'
; 2.1. Rule 'dnf-rule-constant'
(let [dnf (expression-dnf (constant 0) {})] (test/is (= dnf (constant 0))))
; 2.2. Rule 'dnf-rule-negation-of-constant'
(let [dnf (expression-dnf (negation (constant 0)) {})] (test/is (= dnf (constant 1))))
(let [dnf (expression-dnf (negation (constant 1)) {})] (test/is (= dnf (constant 0))))
; 2.3. Rule 'dnf-rule-variable-with-substitution'
(let [dnf (expression-dnf (variable :x1) {:x1 1})] (test/is (= dnf (constant 1))))
; 2.4. Rule 'dnf-rule-variable-without-substitution'
(let [dnf (expression-dnf (variable :x1) {:x2 1})] (test/is (= dnf (variable :x1))))
; 2.5. Rule 'dnf-rule-conjunction'
(let [var1 (variable :x1) n_var2 (negation (variable :x2)) var3 (variable :x3) n_var4 (negation (variable :x4)) dnf (expression-dnf (conjunction (disjunction var1 n_var2) (disjunction n_var4 var3)) {})]
  (test/is
    (and
      (disjunction? dnf)
      (every? conjunction? (disjunction-expressions dnf))
      (let [pairs (apply hash-set (map #(apply hash-set (conjunction-expressions %)) (disjunction-expressions dnf)))]
        (=
          pairs
          (hash-set
            (hash-set var1 var3)
            (hash-set var1 n_var4)
            (hash-set n_var2 var3)
            (hash-set n_var2 n_var4)))))))
; 2.6. Rule 'dnf-rule-disjunction'
(let [dnf (expression-dnf (disjunction (variable :x1) (negation (variable :x2)) (disjunction (variable :x3) (variable :x4))) {})] (test/is (and (disjunction? dnf) (= (hash-set (variable :x1) (negation (variable :x2)) (variable :x3) (variable :x4)) (apply hash-set (disjunction-expressions dnf))))))
; 2.7. Rule 'dnf-rule-implication'
(let [dnf (expression-dnf (implication (variable :x1) (variable :x2)) {})] (test/is (and (disjunction? dnf) (= (hash-set (negation (variable :x1)) (variable :x2)) (apply hash-set (disjunction-expressions dnf))))))
; 2.8. Rule 'dnf-rule-negation-of-variable-with-substitution'
(let [dnf (expression-dnf (negation (variable :x1)) {:x1 1})] (test/is (= dnf (constant 0))))
(let [dnf (expression-dnf (negation (variable :x1)) {:x1 0})] (test/is (= dnf (constant 1))))
; 2.9. Rule 'dnf-rule-negation-of-variable-without-substitution'
(let [dnf (expression-dnf (negation (variable :x1)) {:x2 1})] (test/is (= dnf (negation (variable :x1)))))
; 2.10. Rule 'negation-of-conjunction'
(let [dnf (expression-dnf (negation (conjunction (variable :x1) (variable :x2))) {})] (test/is (and (disjunction? dnf) (= (hash-set (negation (variable :x1)) (negation (variable :x2))) (apply hash-set (disjunction-expressions dnf))))))
; 2.11. Rule 'negation-of-disjunction'
(let [dnf (expression-dnf (negation (disjunction (variable :x1) (variable :x2))) {})] (test/is (and (conjunction? dnf) (= (hash-set (negation (variable :x1)) (negation (variable :x2))) (apply hash-set (conjunction-expressions dnf))))))
; 2.12. Rule 'negation-of-implication'
(let [dnf (expression-dnf (negation (implication (variable :x1) (variable :x2))) {})] (test/is (and (conjunction? dnf) (= (hash-set (variable :x1) (negation (variable :x2))) (apply hash-set (conjunction-expressions dnf))))))
; 2.13. Rule 'negation-of-negation'
(let [dnf (expression-dnf (negation (negation (variable :x1))) {})] (test/is (= dnf (variable :x1))))



