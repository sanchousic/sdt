(ns dnf
  (:use clojure.set)
  (:use expression)
  (:use tools))

; 1. Tools for DNFs
; 1.1. 'dnf?'
(defn dnf? [expr]
  (or (conjunct? expr) (and (disjunction? expr) (every? conjunct? (disjunction-expressions expr)))))

; 1.2. To get the conjuncts of the DNF
(defn dnf-conjuncts [dnf]
  {:pre [(dnf? dnf)]}
  (cond
    (conjunct? dnf) (list dnf)
    (disjunction? dnf) (disjunction-expressions dnf)))

; 1.3. To build a DNF of two DNFs
(defn dnfs-unite [dnf1 dnf2]
  {:pre [(and (dnf? dnf1) (dnf? dnf2))]}
  (let [conjuncts1 (dnf-conjuncts dnf1) conjuncts2 (dnf-conjuncts dnf2) conjuncts-united (for [conjunct1 conjuncts1 conjunct2 conjuncts2] (conjuncts-unite conjunct1 conjunct2))]
    (if (= (count conjuncts-united) 1) (first conjuncts-united) (apply disjunction conjuncts-united))))

; 1.4. To normalize DNF
;   Explanation
;     a. (0 v (v K_i)) ~ (v K_i)
;     b. (1 v (v K_i)) ~ 1
;     c. (K v K v (v K_i)) -> K v (v K_i)
;     d. (value v (not value) v (v K_i)) -> 1
;     e. Empty dnf ~ 0
(defn dnf-normalize [dnf]
  {:pre [(dnf? dnf)]}
  (let [n_conjuncts_without_0 (filter #(not (and (constant? %) (= (constant-value %) 0))) (map #(conjunct-normalize %) (dnf-conjuncts dnf)))]
    (if (some #(and (constant? %) (= (constant-value %) 1)) n_conjuncts_without_0)
      (constant 1)
      (let
        [unique_n_conjuncts
         (reduce
           (fn [conjuncts conjunct]
             (if
               (some
                 #(and
                    (= (conjunct-set-of-variables-names conjunct) (conjunct-set-of-variables-names %))
                    (= (conjunct-set-of-negated-variables-names conjunct) (conjunct-set-of-negated-variables-names %)))
                 conjuncts)
               conjuncts
               (cons conjunct conjuncts)))
           (list)
           n_conjuncts_without_0)]
        (if
          (some
            (fn [conjunct]
              (and (variable? conjunct) (some #(and (negation? %) (variable? (negation-expression %)) (= (variable-name conjunct) (variable-name (negation-expression %)))) unique_n_conjuncts)))
              unique_n_conjuncts)
          (constant 1)
          (cond
            (= (count unique_n_conjuncts) 0) (constant 0)
            (= (count unique_n_conjuncts) 1) (first unique_n_conjuncts)
            :else (apply disjunction unique_n_conjuncts)))))))

; 2. To get a DNF of the expression
; 2.1. Declaration
(declare expression-dnf)

; 2.2. Rules. We will use them in the definition of 'expression-dnf'
(def dnf-rule-constant
  [(fn [expr _] (constant? expr))
   (fn [expr _] expr)])

(def dnf-rule-negation-of-constant
  [(fn [expr _] (and (negation? expr) (constant? (negation-expression expr))))
   (fn [expr _] (constant (- 1 (constant-value (negation-expression expr)))))])

(def dnf-rule-variable-with-substitution
  [(fn [expr vr] (and (variable? expr) (contains? vr (variable-name expr))))
   (fn [expr vr] (constant (get vr (variable-name expr))))])

(def dnf-rule-variable-without-substitution
  [(fn [expr vr] (and (variable? expr) (not (contains? vr (variable-name expr)))))
   (fn [expr _] expr)])

(def dnf-rule-negation-of-variable-with-substitution
  [(fn [expr vr] (and (negation? expr) (let [sub-expr (negation-expression expr)] (and (variable? sub-expr) (contains? vr (variable-name sub-expr))))))
   (fn [expr vr] (expression-dnf (negation (constant (get vr (variable-name (negation-expression expr))))) vr))])

(def dnf-rule-negation-of-variable-without-substitution
  [(fn [expr vr] (and (negation? expr) (let [sub-expr (negation-expression expr)] (and (variable? sub-expr) (not (contains? vr (variable-name sub-expr)))))))
   (fn [expr _] expr)])

(def dnf-rule-disjunction
  [(fn [expr _] (disjunction? expr))
   (fn [expr vr]
     (let [sub-dnfs (map #(expression-dnf % vr) (disjunction-expressions expr))]
       (apply disjunction (reduce (fn [conjuncts-all sub-dnf] (concat conjuncts-all (dnf-conjuncts sub-dnf))) (list) sub-dnfs))))])

(def dnf-rule-conjunction
  [(fn [expr _] (conjunction? expr))
   (fn [expr vr]
     (let [sub-dnfs (map #(expression-dnf % vr) (conjunction-expressions expr))]
       (reduce
         dnfs-unite
         (first sub-dnfs)
         (rest sub-dnfs))))])

(def dnf-rule-implication
  [(fn [expr _] (implication? expr))
   (fn [expr vr]
     (let [sub-exprs (implication-expressions expr)]
       (expression-dnf (disjunction (negation (first sub-exprs)) (second sub-exprs)) vr)))])

(def dnf-rule-negation-of-conjunction
  [(fn [expr _] (and (negation? expr) (conjunction? (negation-expression expr))))
   (fn [expr vr]
     (let [sub-expr (negation-expression expr)]
       (expression-dnf (apply disjunction (map #(negation %) (conjunction-expressions sub-expr))) vr)))])

(def dnf-rule-negation-of-disjunction
  [(fn [expr _] (and (negation? expr) (disjunction? (negation-expression expr))))
   (fn [expr vr]
     (let [sub-expr (negation-expression expr)]
       (expression-dnf (apply conjunction (map #(negation %) (disjunction-expressions sub-expr))) vr)))])

(def dnf-rule-negation-of-implication
  [(fn [expr _] (and (negation? expr) (implication? (negation-expression expr))))
   (fn [expr vr]
     (let [sub-sub-exprs (implication-expressions (negation-expression expr))]
       (expression-dnf (conjunction (first sub-sub-exprs) (negation (second sub-sub-exprs))) vr)))])

(def dnf-rule-negation-of-negation
  [(fn [expr _] (and (negation? expr) (negation? (negation-expression expr))))
   (fn [expr vr]
     (expression-dnf (negation-expression (negation-expression expr)) vr))])

(def dnf-rules
  (list
    dnf-rule-constant
    dnf-rule-negation-of-constant
    dnf-rule-variable-with-substitution
    dnf-rule-variable-without-substitution
    dnf-rule-conjunction
    dnf-rule-disjunction
    dnf-rule-implication
    dnf-rule-negation-of-variable-with-substitution
    dnf-rule-negation-of-variable-without-substitution
    dnf-rule-negation-of-conjunction
    dnf-rule-negation-of-disjunction
    dnf-rule-negation-of-implication
    dnf-rule-negation-of-negation))

; 2.3. Definition
(defn expression-dnf [expr vr]
  (dnf-normalize ((some (fn [rule] (if ((first rule) expr vr) (second rule) false)) dnf-rules) expr vr)))
