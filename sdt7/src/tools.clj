(ns tools
  (:use expression))

; 1. 'atom?'
(defn atom? [expr]
  (or (constant? expr) (variable? expr)))

; 2. 'negation of'
(defn negation-of [verification?]
  #(and (negation? %) (verification? (negation-expression %))))

; 3. Conjunct
; 3.1. 'conjunct?'
(defn conjunct? [expr]
  (or
    (atom? expr)
    ((negation-of atom?) expr)
    (and (conjunction? expr) (reduce (fn [result sub_expr] (and result (or (atom? sub_expr) ((negation-of atom?) sub_expr)))) true (conjunction-expressions expr)))))

; 3.2. 'conjunct-expressions'
(defn conjunct-expressions [expr]
  {:pre [(conjunct? expr)]}
  (cond
    (or (atom? expr) ((negation-of atom?) expr)) (list expr)
    (conjunction? expr) (conjunction-expressions expr)))

; 3.3. 'conjunct-set-of-constants-values'
(defn conjunct-set-of-constants-values [expr]
  {:pre [(conjunct? expr)]}
  (reduce
    (fn [result sub_expr]
      (cond
        (constant? sub_expr) (conj result (constant-value sub_expr))
        ((negation-of constant?) sub_expr) (conj result (- 1 (constant-value (negation-expression sub_expr))))
        :else result))
      (hash-set)
      (conjunct-expressions expr)))

; 3.4. 'conjunct-set-of-variables-names'
(defn conjunct-set-of-variables-names [expr]
  {:pre [(conjunct? expr)]}
  (reduce
    (fn [result sub_expr]
      (if (variable? sub_expr)
        (conj result (variable-name sub_expr))
        result))
    (hash-set)
    (conjunct-expressions expr)))

; 3.5. 'conjunct-set-of-negated-variables-names'
(defn conjunct-set-of-negated-variables-names [expr]
  {:pre [(conjunct? expr)]}
  (reduce
    (fn [result sub_expr]
      (if ((negation-of variable?) sub_expr)
        (conj result (variable-name (negation-expression sub_expr)))
        result))
    (hash-set)
    (conjunct-expressions expr)))

; 3.6. 'conjunct-unite'
(defn conjuncts-unite [expr1 expr2]
  {:pre [(and (conjunct? expr1) (conjunct? expr2))]}
  (apply conjunction (concat (conjunct-expressions expr1) (conjunct-expressions expr2))))

; 3.7. 'conjunct-normalize'
;   Explanation
;     a. (1 & (& 'value_i) ~ (& 'value_i)). We will not use 1-constants
;     b. (0 & (& 'value_i)) ~ 0
;     c.1 ((value & value) & (& 'value_i)) ~ value & (& 'value_i)
;     c.2 ((not value) & (not value)) & (& 'value_i) ~ (not value) & (& 'value_i)
;     d. ((value & (not value)) & (& 'value_i)) ~ 0
;     e. Empty conjunct ~ 1
(defn conjunct-normalize [expr]
  (if (contains? (conjunct-set-of-constants-values expr) 0)
    (constant 0)
    (let [names_of_variables (conjunct-set-of-variables-names expr) names_of_negated_variables (conjunct-set-of-negated-variables-names expr)]
      (if (some #(contains? names_of_negated_variables %) names_of_variables)
        (constant 0)
        (let
          [normalized_conjunct-expressions (concat (map #(variable %) names_of_variables) (map #(negation (variable %)) names_of_negated_variables))]
          (cond
            (= (count normalized_conjunct-expressions) 0) (constant 1)
            (= (count normalized_conjunct-expressions) 1) (first normalized_conjunct-expressions)
            :else (apply conjunction normalized_conjunct-expressions)))))))