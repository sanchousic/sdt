(ns main
  (:use expression)
  (:use tools)
  (:use dnf))

(println "Examples")
(println "-------")
(let
  [expr   (conjunction (disjunction (negation (variable :a)) (variable :b)) (conjunction (variable :a) (negation (variable :b))) (conjunction (variable :c) (variable :d)))
   vr     {}]
  (println (clojure.string/replace (str (expression-dnf expr vr)) ":expression/" "")))

(let
  [expr   (conjunction (disjunction (negation (variable :a)) (variable :b)) (conjunction (variable :a) (negation (variable :e))) (conjunction (variable :c) (variable :d)))
   vr     {:b 1}]
  (println (clojure.string/replace (str (expression-dnf expr vr)) ":expression/" "")))

(let
  [expr   (conjunction (implication (variable :c) (variable :d)) (variable :a) (variable :b))
   vr     {}]
  (println (clojure.string/replace (str (expression-dnf expr vr)) ":expression/" "")))

(let
  [expr   (disjunction (conjunction (variable :a) (variable :b)) (conjunction (variable :a) (variable :b)) (negation (variable :a)) (variable :a))
   vr     {}]
  (println (clojure.string/replace (str (expression-dnf expr vr)) ":expression/" "")))

(let
  [expr   (disjunction (conjunction (disjunction (variable :a) (variable :e)) (variable :b)) (conjunction (variable :a) (variable :b)) (negation (variable :c)) (variable :a))
   vr     {}]
  (println (clojure.string/replace (str (expression-dnf expr vr)) ":expression/" "")))
(println "-------")
(println ":D")
