(ns expression)


; Constant
(defn constant [value]
  {:pre [(number? value)]}
  (list ::const (if (== value 0) 0 1)))

(defn constant? [expr]
  (= ::const (first expr)))

(defn constant-value [c]
  {:pre [(constant? c)]}
  (second c))

; Variable
(defn variable [name]
  {:pre [(keyword? name)]}
  (list ::variable name))

(defn variable? [expr]
  (= ::variable (first expr)))

(defn variable-name [v]
  {:pre [(variable? v)]}
  (second v))

; Conjunction
(defn conjunction [expr & rest]
  (cons ::conjunction (cons expr rest)))

(defn conjunction? [expr]
  (= ::conjunction (first expr)))

(defn conjunction-expressions [expr]
  {:pre [(conjunction? expr)]}
  (rest expr))

; Disjunction
(defn disjunction [expr & rest]
  (cons ::disjunction (cons expr rest)))

(defn disjunction? [expr]
  (= ::disjunction (first expr)))

(defn disjunction-expressions [expr]
  {:pre [(disjunction? expr)]}
  (rest expr))

; Implication
(defn implication [expr1 expr2]
  (list ::implication expr1 expr2))

(defn implication? [expr]
  (= ::implication (first expr)))

(defn implication-expressions [expr]
  {:pre [(implication? expr)]}
  (rest expr))

; Negation
(defn negation [expr]
  (list ::negation expr))

(defn negation? [expr]
  (= ::negation (first expr)))

(defn negation-expression [expr]
  {:pre [(negation? expr)]}
  (second expr))
